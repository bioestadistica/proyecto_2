---
title: "Práctico 2"
author: "Pablo Contreras, Sonia Espinoza y Sebastián Flores"
date: "5/26/2022"
output:
  pdf_document: default
  word_document: default
subtitle: Curso _Bioestadística I_ (`CBBIOEST1-1`)
editor_options:
  markdown:
    wrap: 72
---

**Instrucciones:** mantener los grupos del práctico anterior y
entregar: 1. Este mismo archivo `.Rmd` modificado incluyendo con sus
respuestas y con nombre haciendo alusión a las y los integrantes del
grupo. 2. El archivo `doc` o `pdf` generado con el archivo `Rmd`
enviado.

**Plazo de entrega: jueves 26**

## 0. Preparación de librerías

```{r}
#Se consulta si existen en el ambiente local utilizando require y si es falso se instalará el package
if(!require(tidyverse)){install.packages("tidyverse")}
if(!require(modeest)){install.packages("modeest")}
if(!require(lmtest)){install.packages("lmtest")}
if(!require(car)){install.packages("car")}
#Se cargan librerías
library(tidyverse)
library(modeest)
library(lmtest)
library(car)
library(dplyr, warn.conflicts = FALSE)
```

## 1. Preparación de los datos

**[1.1]** Lean el archivo de datos `weight-height.csv` y generen un
`data.frame` llamado `population` que guarde la información del archivo.

```{r}
population <- read.csv("weight-height.csv")
#Se "adjunta" la base de datos en la ruta de búsqueda de R para acceder a sus variables de forma simple
attach(population)
```

**[1.2]** Incluyan en dicho `data.frame` la columna `IMC`, como lo hizo
en práctico anterior.

```{r}
#Se utiliza formula para calcular BMI = (Pounds*703)/(inches^2)
population$IMC <- (Weight*703)/((Height)^2)
detach(population)
population[1:10,]
attach(population)
```

Fuente: [BMI
Calculator](https://mercer-health.com/services/weight-management-center/bmi-calculator#:~:text=Body%20Mass%20Index%2C%20or%20BMI,inches%20x%20height%20in%20inches).)

## 2. Análisis de datos numéricos

**[2.1]** Decidan si hay evidencia para rechazar que el IMC es una
variable aleatoria (v.a.) normal. *Recuerden argumentar su respuesta y
escribir los códigos que sean necesarios.*

**Respuesta:** (Escriban aquí su respuesta).

**[2.1.1]** **Comparando moda respecto a promedio y mediana**

```{r}
#Se obtiene la moda de la variable IMC
moda_IMC <- mlv(IMC, method = "meanshift")[1]
moda_IMC
#Se obtiene el resumen de la variable IMC
summary(IMC)
```

**Conclusión**: La moda es de 27.27 y comparándolo con el promedio =
25.47 y mediana = 25.73 son distintos por lo que se puede deducir
inicialmente que el IMC no es una variable aleatoria normal pero no es
lo suficiente para confirmarlo.

**[2.1.2]** **Gráfico cuantil-cuantil**

```{r}
qqPlot(IMC, xlab = "Cuantiles",
   ylab = "IMC", main = "Gráfico cuantil-cuantil IMC")
```

**Conclusión**: Podemos ver que los puntos en las colas no están
exactamente a lo largo de la línea recta, debido a que se encuentran los
IMC tanto de hombres como mujeres, pero de todos modos la mayor parte de
los datos de la muestra parecen estar distribuidos normalmente.

**[2.1.3]** **Test Shapiro-Wilk**

Para confirmarlo se realizará el test Shapiro-Wilk, el problema es que
el tamaño de la muestra actual es muy grande por lo que se creará un
vector que contenga los p-valor de cada test a partir de muestras
aleatorias de 100 personas incluyendo ambos géneros.

```{r}
#Se inicializa vector vacío
shapiro_100_n <- vector()
#Se crea un ciclo en el que 10 veces
for (i in 1:10) {
  #Se creará a partir del IMC una muestra aleatoria de 100 personas
  smpl <- sample(IMC, size = 100)
  #Se almacenará el p-valor del test shapiro-wilk en el vector en la posición i y se imprime en pantalla
  print(shapiro_100_n[i] <- shapiro.test(smpl)$p.value)
}
```

**Conclusión**: En base a los resultados obtenidos utilizando el test de
shapiro-wilk para 100 muestras aleatorias incluyendo ambos géneros se
puede aceptar la hipótesis nula que el IMC es una variable aleatoria
normal ya que los p-valores en la mayoría de los casos se encuentran por
sobre el nivel de significancia a = 0.05. Si utilizamos muestras de
datos muy grandes el test es muy sensible a casos atípicos y se
esperaría que el p-valor sea mucho menor que el nivel de significancia.

**[2.1.4]** Caso opcional ¿Qué hubiese pasado si en el test shapiro-wilk
se utiliza el máximo de tamaño de muestra permitido?

```{r}
#Se crea una muestra aleatoria de 5000 personas.
muestra_IMC <- sample(IMC, size = 5000)
#Se ejecuta el test
shapiro.test(muestra_IMC)
```

**Conclusión**: El p-value es menor a 2.2e-16 por lo que se rechaza la
hipótesis nula que el IMC es una variable aleatoria normal y se acepta
que el IMC no es una variable aleatoria normal. Este resultado se ve
afectado por el tamaño de la muestra y si se hubiese aceptado la
hipótesis nula con este resultado todos los test utilizados en los
siguientes ejercicios habrían sido distintos ya que algunos son
sensibles cuando uno de los grupos no tiene una distribución normal.

**[2.2]** Luego decidan si hay evidencia para rechazar que el IMC sea
una v.a. normal en el subgrupo de personas de sexo masculino, después en
el subgrupo de personas de sexo femenino.

**Respuesta:**

**[2.2.1]** **Preparación de datos**

```{r}
#Se crea un vector a partir de la columna IMC y sexo masculino
imc_hombres <- IMC[Gender == "Male"]
#Se crea el vector a partir de la columna IMC y sexo femenino
imc_mujeres <- IMC[Gender == "Female"]
```

**[2.2.2]** **Análisis subgrupo IMC para personas de sexo masculino**

```{r}
#Se obtiene la moda de la variable IMC de hombres
moda_IMC_H <- mlv(imc_hombres, method = "meanshift")[1]
moda_IMC_H
#Se obtiene el resumen de la variable IMC de 
summary(imc_hombres)
```

**Conclusión**: La moda de la variable IMC en hombres es de 27.45 y
comparándolo con el promedio = 27.53 y mediana = 27.52 son cercanos por
lo que se puede deducir inicialmente que el IMC, para las personas de
sexo masculino, es una variable aleatoria normal pero no es lo
suficiente para confirmarlo.

**[2.2.3]** **Gráfico cuantil-cuantil IMC para personas de sexo
masculino**

```{r}
qqPlot(imc_hombres, xlab = "Cuantiles",
   ylab = "IMC", main = "Gráfico cuantil-cuantil IMC sexo masculino")
```

**Conclusión**: Podemos ver que los puntos se encuentran a lo largo de
la línea recta. Por lo tanto, los datos de la muestra para la variable
IMC en personas de sexo masculino parecen estar distribuidos
normalmente.

**[2.2.4]** **Test Shapiro-Wilk IMC para personas sexo masculino**

Para confirmar la distribución normal en el subgrupo se realizará el
test Shapiro-Wilk a partir de muestras aleatorias de 100 personas.

```{r}
#Se inicializa vector vacío
shapiro_100_n_hombres <- vector()
#Se crea un ciclo en el que 10 veces
for (i in 1:10) {
  #Se creará a partir del subgrupo de la variable  una muestra aleatoria de 100 personas
  smpl <- sample(imc_hombres, size = 100)
  #Se almacenará el p-valor del test shapiro-wilk en el vector en la posición i y se imprime en pantalla
  print(shapiro_100_n_hombres[i] <- shapiro.test(smpl)$p.value)
}
mean(shapiro_100_n_hombres)
```

**Conclusión**: En base a los resultados obtenidos utilizando el test de
shapiro-wilk para 100 muestras aleatorias se puede aceptar la hipótesis
nula que el IMC es una variable aleatoria normal ya que los p-valores se
encuentran en promedio por sobre el nivel de significancia a = 0.05.

**[2.2.5]** **Análisis subgrupo IMC para personas de sexo femenino**

```{r}
#Se obtiene la moda de la variable IMC de hombres
moda_IMC_M <- mlv(imc_mujeres, method = "meanshift")[1]
moda_IMC_M
#Se obtiene el resumen de la variable IMC de 
summary(imc_hombres)
```

**Conclusión**: La moda de la variable IMC en mujeres es de 23.71 y
comparándolo con el promedio = 23.42 y mediana = 23.50 no muy son
cercanos por lo que se puede deducir inicialmente que el IMC, para las
personas de sexo femenino, no es una variable aleatoria normal pero se
aplicará otros métodos para confirmarlo.

**[2.2.6]** **Gráfico cuantil-cuantil IMC para personas de sexo
femenino**

```{r}
qqPlot(imc_mujeres, xlab = "Cuantiles",
   ylab = "IMC", main = "Gráfico cuantil-cuantil IMC sexo femenino")
```

**Conclusión**: Podemos ver que los puntos en las colas se alejan pero
la mayoría se encuentra a lo largo de la línea recta. Por lo tanto, los
datos de la muestra para la variable IMC en personas de sexo femeninos
parecen estar distribuidos normalmente.

**[2.2.7]** **Test Shapiro-Wilk IMC para personas sexo femenino**

Para confirmar la distribución normal en el subgrupo se realizará el
test Shapiro-Wilk a partir de muestras aleatorias de 100 personas.

```{r}
#Se inicializa vector vacío
shapiro_100_n_mujeres <- vector()
#Se crea un ciclo en el que 10 veces
for (i in 1:1000) {
  #Se creará a partir del subgrupo de la variable  una muestra aleatoria de 100 personas
  smpl <- sample(imc_mujeres, size = 100)
  #Se almacenará el p-valor del test shapiro-wilk en el vector en la posición i y se imprime en pantalla
  #print()
  shapiro_100_n_mujeres[i] <- shapiro.test(smpl)$p.value
}
#Promedio p-valor
hist(shapiro_100_n_mujeres)
mean(shapiro_100_n_mujeres)
```

**Conclusión**: En base a los resultados obtenidos utilizando el test de
shapiro-wilk para 100 muestras aleatorias se puede aceptar la hipótesis
nula que el IMC es una variable aleatoria normal ya que los p-valores se
encuentran en promedio por sobre el nivel de significancia a = 0.05.

**[2.3]** ¿Cómo se relacionan los dos puntos anteriores con la
aplicación del t-test para comparar el IMC entre varones y mujeres?

**Respuesta:** Los puntos anteriores son importantes para el uso de
t-test ya que esté requiere que los grupos a evaluar posean una
distribución normal y además el tamaño de la muestra influiría en si
conviene utilizar este tipo de test estadístico, ya que si fuera una
muestra pequeña sin distribución normal no podría utilizarse. Los puntos
anteriores evidencian que se cuenta con normalidad en ambos grupos y el
tamaño de muestra adecuado para utilizarlo

**[2.4]** Decidan si hay evidencia para rechazar de que la varianza del
IMC en varones sea diferente de la varianza del IMC en mujeres (i.e.,
supuesto de homocedasticidad).

**Respuesta:**

**[2.4.1] F-test**

Asumimos previamente que las distribuciones en ambos grupos tiende a la
normalidad por lo que se utilizará el test de Fisher para determinar si
las varianzas son diferentes o no.

```{r}
#Ho -> VarianzaIMCHombres = VarianzaIMCMujeres
#Ha -> VarianzaIMCHombres != VarianzaIMCMujeres
var.test(imc_hombres, imc_mujeres)
#El p-valor es 2.2e-16 por lo que el test encuentra diferencias significativas entre las varianzas de los dos grupos.
```

**Conclusión**: El p-valor es menor a 2.2e-16 por lo que se rechaza la
hipótesis nula que la varianza del IMC en hombres y mujeres son iguales.
Por lo tanto, la evidencia nos permite aceptar que las varianzas del IMC
entre ellos no es la misma.

**[2.4.2] Fligner-Killeen**

```{r}
#Ho -> VarianzaIMCHombres = VarianzaIMCMujeres
#Ha -> VarianzaIMCHombres != VarianzaIMCMujeres

var.test(imc_hombres, imc_mujeres)
#El p-valor es 2.2e-16 por lo que el test encuentra diferencias significativas entre las varianzas de los dos grupos.
```

**Conclusión**: El p-valor es menor a 2.2e-16 por lo que se rechaza la
hipótesis nula que la varianza del IMC en hombres y mujeres son iguales.
Por lo tanto, la evidencia nos permite aceptar que las varianzas del IMC
entre ellos no es la misma.

**[2.4.3] Análisis diagrama de cajas**

Para agregar más evidencia se construirá un diagrama de cajas a partir
de la variable IMC agrupados por género.

```{r}
boxplot(IMC ~ Gender, xlab = "Géneros",
   ylab = "IMC", main = "Análisis IMC", border = c("blue", "red"))
```

**Conclusión**: Analizando el tamaño de la caja por cada género se puede
intuir que la varianza no es la misma.

**[2.5]** ¿Qué argumento o parámetro de entrada (*input*) se debería en
este caso usar en la función `t.test` para aplicar un t-test que sea
adecuado para inferir acerca de la diferencia de medias del IMC entre
varones y mujeres?

**Respuesta:** var.equal es el argumento que indica que las varianzas de
ambos grupos para el t-test se considerará que son iguales o no. Como
criterio adicional, el tamaño de la muestra es importante para la
determinación de la diferencia de medias, ya que es grande (10000) y
cada grupo posee la misma cantidad (5000) por lo que podemos utilizar el
var.equal = TRUE.

```{r}
t.test(imc_hombres, imc_mujeres, var.equal = FALSE)

```

## 3. Análisis de datos categóricos

**[3.1]** Incluyan una columna adicional `rangoIMC` que sea de tipo
`factor` y cuyos datos sean "Insuficiente", "Saludable", "Sobrepeso" u
"Obeso" según los valores de IMC y la
[clasificación](https://www.cdc.gov/healthyweight/spanish/assessing/index.html#:~:text=Si%20su%20IMC%20es%20menos,dentro%20del%20rango%20de%20obesidad)
del Centro para el Control de la Prevención de Enfermedades.

-   Si su IMC es menos de **18.5**, se encuentra dentro del rango de
    peso **insuficiente**.

-   Si su IMC es entre **18.5** y **24.9**, se encuentra dentro del
    rango de peso **normal** o **saludable**.

-   Si su IMC es entre **25.0** y **29.9**, se encuentra dentro del
    rango de **sobrepeso**.

-   Si su IMC es **30.0** o superior, se encuentra dentro del rango de
    **obesidad**.

```{r}
#Se agrega a la tabla la columna rangoIMC utilizando la función mutate de la librería dplyr
population <- population %>% mutate(rangoIMC= case_when(IMC < 18.5 ~ 'Insuficiente', (IMC >= 18.5 & IMC < 25) ~ 'Saludable', (IMC >= 25 & IMC < 30) ~ 'Sobrepeso', IMC >= 30 ~ 'Obeso'))
detach(population)
attach(population)
```

**[3.2]** Muestren en una tabla la cantidad (i.e., frecuencia absoluta)
de personas que está en cada rango. *Escriban los códigos para esto a
continuación.*

```{r}
#Se define en una nueva tabla a partir del dataframe population y se cuenta por la columna rangoIMC
rangos <- population %>% count(rangoIMC)
#Se cambia el nombre de la columna a frecuencia
names(rangos)[2] <- 'frecuencia'

rangos
```

**[3.3]** Muestren en una tabla el porcentaje de personas que está en
cada rango. *Escriban los códigos para esto a continuación.*

```{r}
#Se define una nueva tabla a partir de la tabla rangos
rangosConPorcentaje <- rangos
#Se agrega una nueva columna que representa el % de personas que están en cada rango
rangosConPorcentaje <- rangosConPorcentaje %>%mutate(porcentaje = frecuencia*100/sum(rangosConPorcentaje$frecuencia))

rangosConPorcentaje
```

**[3.4]** Infieran, mediante un test estadístico adecuado, sobre la
diferencia de la proporción de personas con obesidad o sobrepeso entre
hombres y mujeres. *Escriban los códigos para esto a continuación.*

**[3.4.1] Preparación datos hombres con sobrepeso u obesidad**

```{r}
#Tabla formada a partir de las condiciones que la persona sea de genero masculino y el rangoIMC sea obeso o sobrepeso
hombres_sp_o <- population[Gender == "Male" & (rangoIMC == 'Obeso' | rangoIMC == 'Sobrepeso'), ]
#Se calcula el tamaño del subgrupo
nrow(hombres_sp_o)
#El número de hombres que se encuentran en sobrepeso u obesidad es 4773
```

**[3.4.2] Preparación datos mujeres con sobrepeso u obesidad**

```{r}
#Tabla formada a partir de las condiciones que la persona sea de genero femenino y el rangoIMC sea obeso o sobrepeso
mujeres_sp_o <- population[Gender == "Female" & (rangoIMC == 'Obeso' | rangoIMC == 'Sobrepeso'), ]
#Se calcula el tamaño del subgrupo
nrow(mujeres_sp_o)
#El tamaño de la muestra es 1071
```

**[3.4.3] Test de comparación de proporciones**

Con los datos preparados se utiliza el siguiente test para obtener la
diferencia de proporciones de dos muestras, x indica un vector que posee
las frecuencias por genero y n el tamaño de cada muestra por genero.

```{r}
prop.test(x = c(nrow(hombres_sp_o), nrow(mujeres_sp_o)), n = c(length(imc_hombres), length(imc_mujeres)))
```

**Conclusión**: El p-valor del test es **2.2e-16**, el cual es menor que
el nivel de significacia a = 0.05. Concluimos que la proporción de
personas obesas o con sobrepeso es significativamente diferente en los
dos grupos.

**[3.5]** Calcule el OR asociado al factor "ser de sexo femenino en vez
de masculino" respecto a su efecto en "ser obeso en vez de no serlo",
junto a su p-valor.

**Respuesta**

**[3.5.1] Preparación de datos**

```{r}
#Se define una matriz la cual incluye la frecuencia por genero de ser obeso/sobrepeso o no serlo
matrizOR=array(c(nrow(hombres_sp_o), nrow(mujeres_sp_o),length(imc_hombres) - nrow(hombres_sp_o), length(imc_mujeres) - nrow(mujeres_sp_o)),dim=c(2,2))
colnames(matrizOR)=c("Obeso/Sobrepeso+","Obeso/Sobrepeso-")
rownames(matrizOR)=c("H","M")
matrizOR
```

**[3.5.2] Análisis e interpretación**

```{r}
#Proporciones por columna
pc <- prop.table(matrizOR,2)
round(pc,2)
#Se puede confirmar que el 82% de los hombres se encuentran en el rango de sobrepeso/obesidad y el 5% de los hombres se encuentra fuera de dichos rangos.

#Para obtener el Odd Ratio se calcula las proporciones por fila de la matriz
pf <- prop.table(matrizOR,1)
round(pf,4)
#El odd ratio sería la razón entre las frecuencias de los hombres y mujeres que tengan obesidad o sobrepeso
or <- pf[1,1]/pf[2,1]
or
```

**Conclusión**: El odd ratio = 4.45. Por lo tanto, en esta población de
encontrar personas en el rango de sobrepeso u obesidad es casi 4.5 veces
mayor en hombres que en mujeres.

**[3.6]** Muestren en una tabla la cantidad (i.e., frecuencia absoluta)
de personas en cada rango según el sexo.

**[3.6.1] Preparación de datos**

Se genera una tabla de contigencia a partir de las categorias
entregandonos el numero de casos observado para cada subgrupo.

```{r}
table(rangoIMC, Gender)
```

**[3.6.1] Análisis a través de gráfico de barras**

```{r}
ggplot(population) +
  aes(x = rangoIMC, fill = Gender) +
  geom_bar(position = "dodge")
```

A partir del gráfico podemos determinar que hay dependencia entre el
sexo y el rango IMC, ya que los hombres tienden a encontrarse en el
rango sobrepeso y obeso, en este último 0 mujeres por lo que se confirma
la dependencia entre las variables.

**[3.7]** ¿Son las variables `sexo` y `rangoIMC` estadísticamentes
independientes? *Recuerden argumentar su respuesta y escribir los
códigos que sean necesarios.*

**Respuesta**:

**[3.7.1] Análisis a través test Chi-cuadrado**

Para determinar si las variables sexo y rangoIMC son estadisticamentes
independientes se utilizará el test estadístico Chi cuadrado.

```{r}
chisq.test(table(rangoIMC, Gender))
```

**[3.7.1] Análisis a través test exacto de Fisher**

```{r}
fisher.test(table(rangoIMC, Gender))
```

Conclusión: En ambos test el p-valor es 2.2e-16, lo cual es menor al
nivel de significancia a = 0.05 por lo que se rechaza la hipótesis nula.
Por lo tanto, significa que hay dependencia entre el sexo y en rangoIMC

**[3.8]** ¿Hace sentido usar la misma definición de IMC en varones y en
mujeres? ¿Habrá otro factor oculto? Comenten. *Recuerden argumentar su
respuesta y escribir los códigos que sean necesarios.*

**Respuesta**

La definición de IMC depende de otros factores que no están siendo
considerados por ejemplo la edad. Esto debido a que en la muestra se
presentan datos atípicos (IMC = 14.5) que pueden influenciar en los
resultados de los test y por ende en nuestra toma de decisión y en el
caso que hubiesen menores de edad se utilizan parámetros adicionales
para su cálculo y decidir en que rango se encuentra. A su vez, personas
adultos mayores poseen mayor cantidad de grasa corporal por lo que el
IMC puede no representar el estado real de salud de la persona.

Otros factores asociados al estilo de vida pueden ser si la persona
práctica deporte o no, ya que al poseer mayor masa muscular e IMC sobre
25 no implica que debería clasificarse como alguien con sobrepeso.

## 4. Percepción del aprendizaje

Las siguientes dos preguntas no son evaluadas, pero sí nos ayudarían a
conocer desde vuestra mirada cómo van sus procesos de aprendizaje.

**[4.1]** Según vuestra experiencia en el curso y según vuestra opinión
grupal, ¿Qué dirían que es lo más importante que han aprendido hasta
ahora en el curso?

**Respuesta:** Lo más importante ha sido el que podemos actualmente
tomar una muestra y aplicar distintos test e interpretarlos. Esto
permitirá que podamos en cualquier situación que requiera análisis
estadístico, para una toma de decisión por ejemplo, aplicarlo en
nuestras distintas áreas y así afirmar o aceptar lo que aseveremos con
un respaldo.

**[4.1]** Según vuestra experiencia en el curso y según vuestra opinión
grupal, ¿Qué dirían que es lo que aún no tienen claro o les confunde de
lo que han visto hasta ahora en el curso?

**Respuesta:** Al no ser profesionales que trabajen en esta área, lo mas
difícil fue tener mayor claridad al momento de elegir que test
estadístico aplicar ya que el uso de uno u otro depende de otros
factores que pueden o no estar explícitos en el problema y así también
la sensibilidad algunos en base a la cantidad de datos a probar. Queda
claro que es cuestión de tiempo para ir mejorando y tener un ojo
estadístico mucho más preciso.
